import './App.css';
import { Provider } from 'react-redux';
import store from './redux/store';
import AddTaskContainer from './components/AddTaskContainer';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <AddTaskContainer />
      </div>
    </Provider>
  );
}

export default App;
