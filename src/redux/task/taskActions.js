import { ADD_TASK, DONE_TASK, REMOVE_TASK } from "./taskTypes";


export const addTask = (task) => {
    return {
        type: ADD_TASK,
        payload: task,
    }
}

export const doneTask = (task) => {
    return {
        type: DONE_TASK,
        payload: task
    }
}

export const removeTask = (task) => {
    return {
        type: REMOVE_TASK,
        payload: task
    }
}