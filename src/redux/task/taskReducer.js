import { ADD_TASK, DONE_TASK, REMOVE_TASK} from "./taskTypes";

const initialState = []

const taskReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_TASK: return([...state, action.payload])
        case REMOVE_TASK: return(state.filter((data)=> action.payload !== data.task))
        case DONE_TASK: return(state.map((data)=>{
            if(data.task === action.payload){
                data.task = data.task + " DONE"
            }
            console.log("%c Line:14 🌽", "color:#93c0a4",data);
            return data
        }))
        
        default: return state
    }
}

export default taskReducer
