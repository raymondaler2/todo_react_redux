import React, { useState } from "react";
import { connect } from "react-redux";
import { addTask, removeTask, doneTask } from "../redux/task/taskActions";

const AddTaskContainer = (props) => {
    const [task, setTask] = useState()

    const InputHandler = (e) => {
        let { value, id} = e.target

        setTask((prev) => ({
            ...prev,
            [id]: value
        }))
    }

    return(
        <div>
            <input type='text' id='task' onChange={InputHandler}></input>
            <button onClick={()=>{props.addTask(task)}}>Add Task</button>
            <br />
            <table>
                <tbody>
                    {props.task === undefined ? <></>: props.task.map( data => 
                    <tr>
                        <td>{data.task}</td>
                        <td><button onClick={()=>{
                            props.doneTask(data.task)
                            }}>Done Task</button></td>
                        <td><button onClick={()=>{props.removeTask(data.task)}}>Delete Task</button></td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    )
}

const mapStateToProps = state => {   
    return {
        task: state.task
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addTask: task => dispatch(addTask(task)),
        removeTask: task => dispatch(removeTask(task)),
        doneTask: task => dispatch(doneTask(task))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTaskContainer)